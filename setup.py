from setuptools import setup, find_packages

setup(name='tdd-example-app',
      version='0.1',
      description='Python example library',
      author='Brett Alonso',
      author_email='1@1.com',
      url='https://1.com/',
      packages=find_packages()
)
